package com.agiletestingalliance;

//import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
//import org.springframework.ui.ModelMap;


public class BasicTest {
	@Test
        public void testTestGstr() throws Exception{
                final String val = new TestGstr("CP-DOF").gstr();
                assertTrue("Text validation",val.equals("CP-DOF"));
        }

	@Test
        public void testMinMaxScn1() throws Exception{
                final int val = new MinMax().findHigh(10,5);
                assertEquals("Max a",val,10);
        }

	 @Test
        public void testMinMaxScn2() throws Exception{
                final int val = new MinMax().findHigh(1,5);
                assertEquals("Max b",val,5);
        }

	@Test
        public void testMinMaxBar() throws Exception{
                final String val = new MinMax().bar("Test");
        	assertTrue("Text validation",val.equals("Test"));
	}

	@Test
        public void testMinMaxBarNull() throws Exception{
                final String val = new MinMax().bar("");
                assertTrue("Text validation",val.equals(""));
        }


	@Test
        public void testAbout() throws Exception{
                final String val = new AboutCPDOF().desc();
                assertTrue("Text validation",val.contains("CP-DOF"));
        }

	@Test
        public void testDuration() throws Exception{
                final String val = new Duration().dur();
                assertTrue("Text validation Duration",val.contains("corporates"));
        }

	@Test
        public void testDurationInt() throws Exception{
                new Duration().calculateIntValue();
                assertEquals("IntVal",5,5);
        }

	@Test
        public void testUsefulnessDesc() throws Exception{
                final String val = new Usefulness().desc();
                assertTrue("Text validation usefulness",val.contains("transformation"));
        }
	
	@Test
        public void testUsefulnessWF() throws Exception{
                new Usefulness().functionWF();
                assertEquals("IntVal",5,5);
        }


}
